#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//IMMER ZUERST LOAD AUFRUFEN WENN MOVIES.CSV EXISTIERT!!

struct movie_info {
    char Name[50];
    int Year;
    char Director[50];
    struct movie_info *next;
};

typedef struct movie_info* movie;

#define OK       0
#define NO_INPUT 1
#define TOO_LONG 2

movie list;

// Hilfsfunktionen

int getLine(char *prmpt, char *buff, size_t sz) {
    int ch, extra;
    
    // Get line with buffer overrun protection.
    if (prmpt != NULL) {
        printf ("%s", prmpt);
        fflush (stdout);
    }
    if (fgets(buff, (int) sz, stdin) == NULL)
        return NO_INPUT;
    
    // If it was too long, there'll be no newline. In that case, we flush
    // to end of line so that excess doesn't affect the next call.
    if (buff[strlen(buff) - 1] != '\n') {
        extra = 0;
        while (((ch = getchar()) != '\n') && (ch != EOF))
            extra = 1;
        return (extra == 1) ? TOO_LONG : OK;
    }
    
    // Otherwise remove newline and give string back to caller.
    buff[strlen(buff) - 1] = '\0';
    return OK;
}

movie add_movie(char Name[], int Year, char Director[]){
    movie new_movie = (movie) malloc(sizeof(struct movie_info));
    strcpy(new_movie->Name, Name);
    new_movie->Year = Year;
    strcpy(new_movie->Director, Director);
    new_movie->next = NULL;
    if (list == NULL) {
        list = new_movie;
    } else {
        movie m = list;
        while (m->next != NULL) {
            m = m->next;
        }
        m->next = new_movie;
    }
    return new_movie;
}

// Menufunktionen

movie add(){
    char Name[50];
    char Year[5];
    char Director[50];
    movie new_movie;
    
    getLine("Please enter a movie: ", Name, sizeof(Name));
    getLine("Please enter the year in which the movie was released: ", Year, sizeof(Year));
    getLine("Please enter the director: ", Director, sizeof(Director));
    new_movie = add_movie(Name, atoi(Year), Director);
    printf("Movie has been successfully added to the archive!");
    return new_movie;
}

void print(movie list){
    movie m = list;
    int count = 0;
    while (m != NULL) {
        printf("Name: %s, Year: %d, Director: %s\n", m->Name, m->Year, m->Director);
        m = m->next;
        count++;
    }
    printf("Number of movies: %d\n", count);
}

void delete(){
    int found = 0;
    movie prev = NULL;
    movie m = list;
    char Name[50];
    getLine("Please enter the movie you wish to delete: ", Name, sizeof(Name));
    while (m != NULL) {
        if (strcmp(Name, m->Name)==0) {
            if (prev != NULL) {
                prev->next = m->next;
            } else if (m->next == NULL){
                list = NULL;
            } else {
                list = m->next;
            }
            free(m);
            printf("Movie has been successfully deleted!");
            found = 1;
            break;
        }
        prev = m;
        m = m->next;
    }
    if (!found) {
        printf("Movie not found!");
    }
}

void search(movie list){
    int selection;
    movie m = list;
    int count = 0;
    char Name[50];
    int Year = -1;
    char Director[50];
    char buff[5];
    char Keyword[50];
    printf("Search options...\n");
    printf("[1] Name\n");
    printf("[2] Year\n");
    printf("[3] Director\n");
    printf("[4] Keyword\n");
    printf("[0] Exit\n");
    getLine("Please choose option: ", buff, sizeof(buff));
    selection = atoi(buff);
    if (selection > 4) {
        printf("Please enter a valid number to proceed!\n");
        return;
    }
    if (selection == 1){
        getLine("Please enter a search phrase: ", Name, sizeof(Name));
    } else if (selection == 2){
        getLine("Please enter a year: ", buff, sizeof(buff));
        Year = atoi(buff);
    } else if (selection == 3){
        getLine("Please enter a director: ", Director, sizeof(Director));
    } else if (selection == 4){
        getLine("Please enter a keyword: ", Keyword, sizeof(Keyword));
    } else if (selection == 0){
        //system("clear");
    }
    while (m != NULL) {
        if ((selection == 1 && strcmp(Name, m->Name)==0) ||
            (selection == 2 && Year == m->Year) ||
            (selection == 3 && strcmp(Director, m->Director)==0) ||
            (selection == 4 && (strstr(m->Name, Keyword) != NULL ||
                                strstr(m->Director, Keyword) != NULL))){
            count++;
            printf("Name: %s, Year: %d, Director: %s\n", m->Name, m->Year, m->Director);
        }
        m = m->next;
    }
    printf("%d results found!\n", count);
}

void load(){
    char filename[100];
    int rc = getLine("Please enter a file name [default: /tmp/movies.csv]: ",
        filename, sizeof(filename));
    if (rc == NO_INPUT || strlen(filename) == 0)
        strcpy(filename, "/tmp/movies.csv");

    FILE *fh;
    char buff[1000];
    char* token;
    char* running;
    char Name[50];
    int Year = -1;
    char Director[50];
    int count = 0;
    
    fh = fopen(filename, "r");
    if (fh) {
        while (fgets(buff, sizeof(buff), fh) != NULL) {
            buff[strlen(buff) - 1] = '\0';
            running = strdup(buff);
            token = strsep(&running, ",");
            while (isspace(*token)) token++;
            strcpy(Name, token);
            token = strsep(&running, ",");
            while (isspace(*token)) token++;
            Year = atoi(token);
            token = strsep(&running, ",");
            while (isspace(*token)) token++;
            strcpy(Director, token);
            add_movie(Name, Year, Director);
            count++;
            free(running);
        }
        fclose(fh);
        printf("Number of movies read: %d", count);
    } else {
        printf("ERROR: Failed to open file %s", filename);
    }
}

void save() {
    char filename[100];
    int rc = getLine("Please enter a file name [default: /tmp/movies.csv]: ",
        filename, sizeof(filename));
    if (rc == NO_INPUT || strlen(filename) == 0)
        strcpy(filename, "/tmp/movies.csv");
    
    FILE *fh;
    fh = fopen(filename, "w");
    int count = 0;
    movie m = list;
    
    if (fh) {
        while (m != NULL) {
            fprintf(fh, "%s,%d,%s\n", m->Name, m->Year, m->Director);
            m = m->next;
            count++;
        }
        printf("Number of movies written: %d\n", count);
    } else {
        printf("ERROR: Failed to open file %s", filename);
    }
    fclose(fh);
}

int printMenu(){
    char buff[5];
    int selection;
    printf("\nMenu:\n");
    printf("[1] Search\n");
    printf("[2] Add\n");
    printf("[3] Delete\n");
    printf("[4] Print\n");
    printf("[5] Load\n");
    printf("[6] Save\n");
    printf("[0] Exit\n");
    getLine("Please choose: ", buff, sizeof(buff));
    selection = atoi(buff);
    if (selection > 6) {
        printf("Please enter a valid number to proceed!\n");
    }
    if (selection == 1){
        search(list);
    } else if (selection == 2){
        add();
    } else if (selection == 3){
        delete();
    } else if (selection == 4){
        print(list);
    } else if (selection == 5){
        load();
    } else if (selection == 6){
        if (list != NULL) {
            save();
        } else {
            printf("WARNING: Nothing to save!");
        }
    } else if (selection == 0){
        //system("clear");
    }
    return selection;
}


int main(int argc, const char * argv[]) {
    printf("Welcome to your video archive!\n");
    int sel = -1;
    while (sel != 0) {
        sel = printMenu();
    }
}