Stand: 20.12.2015

Wir haben bis jetzt die Funktionen grob erstellt
und mit der Ausgabe angefangen.

Stand: 2.1.2016

Wir haben alle Funktionen weiter ausgeführt und vor allem an der
Ausgabe der add und search Funktionen gearbeitet. Die Eingabe bei der
delete Funktion muss noch geschrieben werden. Zudem erstellen wir eine Datei, 
die alle Filme des Archivs enthält (welche noch einglesen werden muss), 
über die wir dann verwalten können. Wir haben noch eine Funktion names
print erstellt, die alle Filme des Archivs ausdruckt (zur Nachkontrolle).

Stand: 4.1.2016

Alle Funktionen und das Programm sind soweit fertig gestellt. Wir haben unsere "movies.txt" Datei,
die alle Filme des Archivs enthält mit dem Programm verbunden. Die load Funktion
lädt alle Filme ins Archiv und die save Funktion speichert die Filme die neu
hinzugefügt wurden oder gelöscht wurden. Diese zwei Funktionen waren bis jetzt die
größte Heraussvorderung und wir haben viel Hilfe aus dem Internert gebraucht.
Wir haben noch ein keyword search gemacht, welcher eingegebene Stichwörter mit 
den Filmen und Regisseuren vergleicht und alle ausgibt, die dieses Stichwort enthalten.